from telegram.ext import CommandHandler

from bot.db import Admin
from bot.commands import all_command_names

def create_handler():
    return CommandHandler("start", command)

def command(update, context):
    reply_text = 'Hola yo soy el bot mensajeador 🤖\n'

    username = update.effective_user.username
    admins = [admin.username for admin in Admin.select()]
    if username not in admins:
        reply_text += 'Veo que no estás en la lista de admins, no vas a poder usarme. Pedí tu permiso en el canal del hacklab.'
    else:
        reply_text += '\nPara ver todos los comandos disponibles apreta en el " / " cerca de tu botón de enviar mensaje.\n\n'
        reply_text += 'O clickealos de la siguiente lista:\n'
        reply_text += '\n'.join([f'- /{command_name}' for command_name in all_command_names])

    update.message.reply_text(reply_text)
