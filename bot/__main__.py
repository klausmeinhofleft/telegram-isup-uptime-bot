import logging, sys, datetime
from pprint import pprint

# github - https://github.com/python-telegram-bot/python-telegram-bot
# code snippets - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets
# reference - https://python-telegram-bot.readthedocs.io/
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from dotenv import load_dotenv
load_dotenv(verbose=True)

from bot.env import try_get_multiple
#from bot.db import db, recreate
#from bot.job import add_msg_job
#import bot.commands as commands

LOGGING, \
BOT_TOKEN, \
BOTS_CHANNEL, \
UPTIME_OK_STR, \
UPTIME_IS_OK_STR, \
IS_DOWN_SECS, \
ALERT_USER_ID, \
ALERT_COOLDOWN_SECS = try_get_multiple('LOGGING, BOT_TOKEN, BOTS_CHANNEL, UPTIME_OK_STR, UPTIME_IS_OK_STR, IS_DOWN_SECS, ALERT_USER_ID, ALERT_COOLDOWN_SECS')

LOGGING = LOGGING.lower() in ['1', 'true', 'yes', 'y']
IS_DOWN_SECS = int(IS_DOWN_SECS)
ALERT_COOLDOWN_SECS = int(ALERT_COOLDOWN_SECS)

poll_job_name = 'poll_job_name'
poll_job_interval = 22
send_received_cooldown = 3

# creamos la DB si no tiene data
#if not db.get_tables():
#    recreate(force=True)

# Enable logging
if LOGGING:
    import traceback
    # a mejorar siguiendo: https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets#an-good-error-handler
    #logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level=logging.INFO)
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level=logging.ERROR)
    logger = logging.getLogger(__name__)
    def error(update, context):
        """Log Errors caused by Updates."""
        trace = "".join(traceback.format_tb(sys.exc_info()[2]))
        logger.error('Update %s caused error: %s. Traceback:\n%s', update.update_id if update else '-', context.error, trace)
        logger.error('%s %s', type(context.error), str(context.error))
        try: update.message.reply_text(f'📛 Ups, me crashié!')
        except Exception: pass

'''def poll_wiki_thread(context):
    print('poll wiki messages...')
    if not queue.empty():
        print('new wiki messages!')
        msgs = []
        while 1:
            msg = queue.get()
            msgs.append(msg)
            if queue.empty():
                break
        msgs_str = '\n'.join(msgs)
        #context.bot.send_message(chat_id=msg.grupo, text=f'Errores detectodos:\n{msgs_str}', parse_mode='MarkdownV2')
        #context.bot.send_message(chat_id='https://t.me/saio_nara', text=f'Errores detectodos:\n{msgs_str}')
        #context.bot.send_message(chat_id='saio_nara', text=f'Errores detectodos:\n{msgs_str}')
        context.bot.send_message(chat_id='@grupo_de_testeo', text=f'Errores detectodos:\n{msgs_str}')'''

def uptime_ok_received(update, context):
    msg_text = update.channel_post.text if update.channel_post else None
    print('msg new group', msg_text)

    now = datetime.datetime.now()
    #2021-04-11T22:57:57.328701
    now_str = now.isoformat()[:10+1+8]

    date_last_sent = context.bot_data['uptime-received-last-sent']
    date_delta = now - date_last_sent
    # 0:00:03.000376 | total_seconds, microseconds, seconds
    #date_delta_str = date_delta.strftime('%M:%S')
    date_delta_secs = round(date_delta.total_seconds())

    if date_delta_secs > send_received_cooldown:
        context.bot_data['uptime-received-last-sent'] = now
        try:
            context.bot.send_message(chat_id=BOTS_CHANNEL, text=f'[{now_str}] {UPTIME_OK_STR} received')
        except telegram.error.Unauthorized as e:
            # Forbidden: bot is not a member of the supergroup chat
            return print(f'ERROR FATAL: sin acceso a chat {BOTS_CHANNEL}')

    context.bot_data['uptime-last-seen'] = now

    jobs = context.job_queue.get_jobs_by_name(poll_job_name)
    for j in jobs: j.schedule_removal()
    context.job_queue.run_repeating(check_uptime, interval=poll_job_interval, name=poll_job_name)
    print('check_uptime thread reiniciado')

def check_uptime(context):
    print('check_uptime...')

    now = datetime.datetime.now()
    #2021-04-11T22:57:57.328701
    now_str = now.isoformat()[:10+1+8]

    date_last_seen = context.bot_data['uptime-last-seen']
    date_delta = now - date_last_seen
    # 0:00:03.000376 | total_seconds, microseconds, seconds
    #date_delta_str = date_delta.strftime('%M:%S')
    date_delta_secs = round(date_delta.total_seconds())

    if date_delta_secs <= 60:
        date_delta_str = f'{date_delta_secs}s'
    elif date_delta_secs <= 60*60:
        date_delta_mins = round(date_delta.total_seconds()/60)
        date_delta_str = f'{date_delta_mins}m'
    else:
        date_delta_hs = round(date_delta.total_seconds()/(60*60))
        date_delta_str = f'{date_delta_hs}h'

    if date_delta_secs >= IS_DOWN_SECS:
        date_last_alert = context.bot_data['alert-last-sent']
        date_alert_delta = now - date_last_alert
        if date_alert_delta.total_seconds() > ALERT_COOLDOWN_SECS:
            context.bot.send_message(chat_id=ALERT_USER_ID, text=f'¡Uptime bot está caído hace {date_delta_str}!')
            context.bot_data['alert-last-sent'] = now

    try:
        context.bot.send_message(chat_id=BOTS_CHANNEL, text=f'[{now_str}] {UPTIME_IS_OK_STR} (last seen {date_delta_str} ago)')
    except telegram.error.Unauthorized as e:
        # Forbidden: bot is not a member of the supergroup chat
        return print(f'ERROR FATAL: sin acceso a chat {BOTS_CHANNEL}')

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    #https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.updater.html
    updater = Updater(BOT_TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    bot_username = dp.bot.name
    print(f'Corriendo como bot {bot_username}')

    try:
        # user id se obtiene hablándole a @userinfobot
        dp.bot.send_message(chat_id=ALERT_USER_ID, text=f'¡Hola, estoy arriba!')
    except telegram.error.BadRequest:
        # Chat not found
        return print(f'ERROR FATAL: sin comunicación con usuarix de alerta id={ALERT_USER_ID}')

    #breakpoint()

    if LOGGING:
        # log all errors
        dp.add_error_handler(error)

    # on different commands - answer in Telegram
    #dp.add_handler(CommandHandler("help", help))
    #for command_handler in commands.all_command_handlers:
    #    dp.add_handler(command_handler)

    # Filters -https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.filters.html#telegram.ext.filters.Filters
    # Filters.chat - https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.filters.html#telegram.ext.filters.Filters.chat
    #dp.add_handler(MessageHandler(Filters.chat('@grupo_de_testeo'), callback_method))
    # este es el que va
    #dp.add_handler(MessageHandler(Filters.chat(username='@grupo_de_testeo'), callback_method))
    #dp.add_handler(MessageHandler(Filters.chat(username='@testchan6655'), callback_method))
    #dp.add_handler(MessageHandler(Filters.chat(username=BOTS_CHANNEL) & Filters.regex(bot_username) & Filters.regex(UPTIME_OK_STR), callback_method))
    dp.add_handler(MessageHandler(Filters.chat(username=BOTS_CHANNEL) & Filters.regex(UPTIME_OK_STR), uptime_ok_received))
    #dp.add_handler(MessageHandler(Filters.chat(username='@grupo_de_testeo') & Filters., callback_method))
    #dp.add_handler(MessageHandler(Filters.chat_type.group, callback_method))
    #dp.add_handler(MessageHandler(Filters.all, callback_method))
    #dp.add_handler(MessageHandler(Filters.text, callback_method))
    #dp.add_handler(MessageHandler(Filters.text & Filters.chat_type.groups, callback_method))
    #dp.add_handler(CommandHandler()
    print('handler de grupo agregado')

    # Start the Bot
    try:
      updater.start_polling()
    except telegram.error.Unauthorized as e:
      print('Api token inválido. Err:', str(e))
      sys.exit(1)

    # arrancamos el job
    #add_msg_job(updater)

    dp.bot_data['uptime-last-seen'] = datetime.datetime.now()
    dp.bot_data['alert-last-sent'] = datetime.datetime(1900, 1, 1)
    dp.bot_data['uptime-received-last-sent'] = datetime.datetime(1900, 1, 1)

    #updater.job_queue.run_once(check_alert_user, 2)
    # run_repeating - https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.jobqueue.html#telegram.ext.JobQueue.run_repeating
    updater.job_queue.run_repeating(check_uptime, interval=poll_job_interval, name=poll_job_name)
    print('check_uptime thread encolado')
    #print('NOOOOOOOcheck_uptime thread encolado')

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    print('Bot corriendo')
    #https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.updater.html#telegram.ext.Updater.idle
    updater.idle()
    print('Bot cerrado')

if __name__ == '__main__':
    main()
